const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const routes = require('./server/routes');

const env = process.env.NODE_ENV || 'development';

// database
const models = require('./server/database');

// passport
const passport = require('./server/passport');

// middlewares
const errorHandler = require('./server/middlewares/error.middleware');


const app = express();

app.use(cors());

app.set('port', process.env.PORT || 3000);

const swaggerDefinition = {
    info: {
        title: 'EJournal API',
        version: '1.0.0',
    },
    host: (env === 'production') ? 'ejournal-backend.herokuapp.com' : `localhost:${app.get('port')}`,
    basePath: '/',
};

const options = {
    swaggerDefinition,
    apis: ['./server/routes/*.js'],
};

const swaggerSpec = swaggerJSDoc(options);

app.get('/swagger.json', (req, res, next) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(passport.initialize());
app.use(passport.session());

models.sequelize.sync()
    .then(() => console.log('Database synced'))
    .catch((err) => console.log(err, 'Something wrong'));

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.use(routes);

app.use(errorHandler);

app.listen(app.get('port'), () => console.log(`Server started at ${app.get('port')}`));
