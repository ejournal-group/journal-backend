const jwt = require('jsonwebtoken');
const fs = require('fs');

const cert = fs.readFileSync('private.pem');

/**
 *
 * @param token
 * @returns {Promise<any>}
 */
module.exports.verify = function verify(token) {
    return new Promise((resolve, reject) => {
        jwt.verify(token, cert, (err, decoded) => {
            if (err) {
                reject(err);
            } else {
                resolve(decoded);
            }
        });
    });
};

module.exports.sign = function sign(object, options) {
    return jwt.sign(object, cert, options);
};
