module.exports = class HttpError extends Error {
    /**
     * @param {Number} error.status
     * @param {String} error.message
     */
    constructor(status, message) {
        super(message);

        this.status = status;

        Error.captureStackTrace(this, HttpError);
    }
};
