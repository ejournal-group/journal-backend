const db = require('../database');
const { sign, verify } = require('../utils/jwt');

/**
 * @param userId {number}
 * @return {object}
 */
async function generateTokens(userId) {
    const accessToken = await sign({ id: userId }, { expiresIn: '15m' });
    const refreshToken = await sign({ id: userId }, { expiresIn: '3d' });

    return {
        token: accessToken,
        refresh: refreshToken,
    };
}

async function refreshAccessToken(refreshToken) {
    const decoded = await verify(refreshToken);
    const { id: userId } = decoded;

    const user = await db.User.findOne({
        where: { id: userId },
        attributes: ['id'],
    });

    if (user) {
        return generateTokens(user.id);
    }
    throw new Error('Refresh token invalid');
}

module.exports = {
    generateTokens,
    refreshAccessToken,
};
