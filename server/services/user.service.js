const Op = require('sequelize').Op;
const db = require('../database');

async function getById(id) {
    return db.User.findOne({
        where: {
            id,
        },
        attributes: ['id', 'email', 'name', 'surname', 'icon'],
    });
}

async function findUser(matchString = '', journalId = null, size = null) {
    const request = {
        where: {
            email: {
                [Op.substring]: matchString,
            },
        },
        attributes: ['id', 'email', 'name', 'surname', 'icon'],
    }
    if (journalId !== null) {
        request.include = {
            model: db.Journal,
            as: 'journals',
            required: false,
            where: {
                id: journalId,
            },
            attributes: ['id'],
        }
    }
    if (size !== null) {
        request.limit = size;
    }
    return db.User.findAll(request)
        .then((data) => {
            const parsedData = JSON.parse(JSON.stringify(data));
            return parsedData
                .filter((item) => (item.journals ? !item.journals.length : true))
                .map((item) => {
                    const { journals = [], ...rest } = item;
                    return {
                        ...rest,
                    }
                })
        });
}

async function changeEmail(id, email) {
    return db.User.update(
        { email },
        {
            where: {
                id,
            },
        },
    );
}

async function changePassword(id, password) {
    return db.User.update(
        { password },
        {
            where: {
                id,
            },
        },
    );
}

async function changeIcon(id, icon) {
    return db.User.update(
        { icon },
        {
            where: {
                id,
            },
        },
    );
}

async function changeUser(id, icon, name, surname, email) {
    return db.User.update(
        {
            icon, name, surname, email,
        },
        {
            where: {
                id,
            },
        },
    );
}

module.exports = {
    getById,
    findUser,
    changeEmail,
    changePassword,
    changeIcon,
    changeUser,
};
