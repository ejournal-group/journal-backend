const db = require('../database');

async function createLesson(journalId, date, userId) {
    const data = await db.UserJournal.findOne({
        where: {
            journalId,
            userId,
            isOwner: true,
        },
    });

    if (!data) { throw new Error('No access'); }

    const newLesson = {
        date,
        journalId,
    };

    return db.Lesson.create(newLesson);
}

async function addMark(journalId, userId, lessonId, value, ownerId) {
    const data = await db.UserJournal.findOne({
        where: {
            journalId,
            userId: ownerId,
            isOwner: true,
        },
    });

    if (!data) { throw new Error('No access'); }

    const userData = await db.UserJournal.findOne({
        where: {
            journalId,
            userId,
        },
    });

    if (!userData) { throw new Error('No such member'); }

    const newMark = {
        value,
        lessonId,
        userJournalId: userData.id,
    };

    const { id } = await db.Mark.create(newMark);
    return db.Mark.findOne({
        where: {
            id,
        },
        attributes: ['id', 'value'],
    });
}

async function updateMark(journalId, markId, value, ownerId) {
    const data = await db.UserJournal.findOne({
        where: {
            journalId,
            userId: ownerId,
            isOwner: true,
        },
    });

    if (!data) { throw new Error('No access'); }

    await db.Mark.update(
        { value },
        {
            where: {
                id: markId,
            },
        },
    );

    return db.Mark.findOne({
        where: {
            id: markId,
        },
        attributes: ['id', 'value'],
    });
}

async function removeMark(journalId, markId, ownerId) {
    const data = await db.UserJournal.findOne({
        where: {
            journalId,
            userId: ownerId,
            isOwner: true,
        },
    });

    if (!data) { throw new Error('No access'); }

    return db.Mark.destroy({
        where: {
            id: markId,
        }
    });
}

module.exports = {
    createLesson,
    addMark,
    updateMark,
    removeMark,
};
