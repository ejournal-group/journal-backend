const db = require('../database');

async function getOne(journalId, userId) {
    const data = await db.UserJournal.findOne({
        where: {
            journalId,
            userId,
        },
    });

    if (!data) { throw new Error('No access'); }

    const journalData = await db.User.findOne({
        attributes: [],
        where: {
            id: userId,
        },
        include: {
            model: db.Journal,
            as: 'journals',
            where: {
                id: journalId,
            },
            through: {
                as: 'isOwner',
                attributes: ['isOwner'],
            },
        },
    });

    const parsedData = JSON.parse(JSON.stringify(journalData));
    const { journals } = parsedData;
    const [rawJournal] = journals;
    const journal = {
        ...rawJournal,
        isOwner: rawJournal.isOwner.isOwner,
    }
    return journal;
}

async function getForUser(userId) {
    const data = await db.User.findOne({
        // raw: true,
        attributes: [],
        where: {
            id: userId,
        },
        include: {
            model: db.Journal,
            as: 'journals',
            through: {
                as: 'isOwner',
                attributes: ['isOwner'],
            },
        },
    });
    const parsedData = JSON.parse(JSON.stringify(data));
    const { journals } = parsedData;
    const userJournals = journals.map((item) => {
        return {
            ...item,
            isOwner: item.isOwner.isOwner,
        };
    });
    return userJournals;
}

async function create(userId, name, description) {
    const { id: journalId } = await db.Journal.create({
        name,
        description,
    });
    await db.UserJournal.create({
        userId,
        journalId,
        isOwner: true,
    });
    return getOne(journalId, userId);
}

async function update(id, data, userId) {
    await db.Journal.update(
        {
            ...data,
        },
        {
            where: {
                id,
            },
        },
    );
    return getOne(id, userId);
}

async function remove(id) {
    return db.Journal.destroy({
        where: {
            id,
        },
    });
}

async function addMember(id, userId) {
    return db.UserJournal.create({
        userId,
        journalId: id,
        isOwner: false,
    });
}

async function removeMember(id, userId) {
    return db.UserJournal.destroy({
        where: {
            userId,
            journalId: id,
        },
    });
}

async function getMembers(journalId, userId) {
    const checkData = await db.UserJournal.findOne({
        where: {
            journalId,
            userId,
        },
    });

    if (!checkData) { throw new Error('No access'); }

    const data = await db.Journal.findOne({
        attributes: [],
        where: {
            id: journalId,
        },
        include: {
            model: db.User,
            as: 'users',
            attributes: ['id', 'email', 'name', 'surname', 'icon'],
            through: {
                as: 'isOwner',
                attributes: ['isOwner'],
            },
        },
    });

    const parsedData = JSON.parse(JSON.stringify(data));
    const { users } = parsedData;
    const journalUsers = users.map((item) => {
        return {
            ...item,
            isOwner: item.isOwner.isOwner,
        };
    });
    return journalUsers;
}

async function getLessons(journalId, userId) {
    const checkData = await db.UserJournal.findOne({
        where: {
            journalId,
            userId,
        },
    });

    if (!checkData) { throw new Error('No access'); }

    return db.Lesson.findAll({
        attributes: ['id', 'date'],
        where: {
            journalId,
        },
    });
}

async function getMarks(journalId, userId) {
    const checkData = await db.UserJournal.findOne({
        where: {
            journalId,
            userId,
        },
    });

    if (!checkData) { throw new Error('No access'); }

    const members = await db.Journal.findOne({
        attributes: [],
        where: {
            id: journalId,
        },
        include: {
            model: db.User,
            as: 'users',
            attributes: ['id', 'email', 'name', 'surname', 'icon'],
            through: {
                as: 'journal',
                attributes: ['id', 'isOwner'],
            },
        }
    }).then((data) => {
        const parsedData = JSON.parse(JSON.stringify(data));
        const { users } = parsedData;
        return users.filter((item) => !item.journal.isOwner);
    });

    const marksData = await db.Lesson.findAll({
        attributes: ['id', 'date'],
        where: {
            journalId,
        },
        include: {
            model: db.Mark,
            as: 'marks',
        }
    });

    const membersWithMarks = members.map((item) => {
        const { journal, ...data } = item;
        const memberMarks = [];

        marksData.forEach((markItem) => {
            const { marks, date } = markItem;
            const userMark = marks.find((mark) => mark.userJournalId === journal.id);
            if (userMark) {
                const { id: markId, value } = userMark;
                memberMarks.push({
                    date,
                    markId,
                    value,
                });
            }
        });


        return {
            ...data,
            marks: memberMarks,
        };
    });

    return membersWithMarks;
}

module.exports = {
    getOne,
    getForUser,
    create,
    update,
    remove,
    addMember,
    removeMember,
    getMembers,
    getLessons,
    getMarks,
};
