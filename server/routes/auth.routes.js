const router = require('express-promise-router')();

const authController = require('../controllers/auth.controller');

/**
* @swagger
* /auth/signin:
*   post:
*     summary: Authorize a user
*     tags:
*       - auth
*     produces:
*       - application/json
*     parameters:
*       - name: email
*         in: formData
*         required: true
*         type: string
*       - name: password
*         in: formData
*         required: true
*         type: string
*     responses:
*       200:
*         schema:
*           type: object
*           properties:
*             token:
*               type: string
*               description: Access token
*             refresh:
*               type: string
*               description: Refresh token
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.post('/signin', authController.login, authController.generateTokens);

/**
* @swagger
* /auth/signup:
*   post:
*     summary: Create new user
*     tags:
*       - auth
*     produces:
*       - application/json
*     parameters:
*       - name: email
*         in: formData
*         required: true
*         type: string
*       - name: password
*         in: formData
*         required: true
*         type: string
*       - name: name
*         in: formData
*         required: true
*         type: string
*       - name: surname
*         in: formData
*         required: true
*         type: string
*     responses:
*       200:
*         schema:
*           type: object
*           properties:
*             token:
*               type: string
*               description: Access token
*             refresh:
*               type: string
*               description: Refresh token
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.post('/signup', authController.register, authController.generateTokens);

/**
* @swagger
* /auth/refresh:
*   post:
*     summary: Refresh access token
*     tags:
*       - auth
*     produces:
*       - application/json
*     parameters:
*       - name: refreshToken
*         in: formData
*         required: true
*         type: string
*     responses:
*       200:
*         schema:
*           type: object
*           properties:
*             token:
*               type: string
*               description: Access token
*             refresh:
*               type: string
*               description: Refresh token
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.post('/refresh', authController.refreshToken);

module.exports = router;
