const router = require('express-promise-router')();

const journalController = require('../controllers/journal.controller');

/**
* @swagger
* /journal/{id}:
*   get:
*     summary: Get journal
*     tags:
*       - journal
*     produces:
*       - application/json
*     parameters:
*       - name: X-Access-Token
*         in: header
*         required: true
*         type: string
*       - name: id
*         in: path
*         required: true
*         type: string
*     responses:
*       200:
*         schema:
*           type: object
*           properties:
*             id:
*               type: integer
*             name:
*               type: string
*             description:
*               type: string
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.get('/:id', journalController.getJournal);

/**
* @swagger
* /journal:
*   post:
*     summary: Create a journal
*     tags:
*       - journal
*     produces:
*       - application/json
*     parameters:
*       - name: X-Access-Token
*         in: header
*         required: true
*         type: string
*       - name: name
*         in: formData
*         required: true
*         type: string
*       - name: description
*         in: formData
*         required: true
*         type: string
*     responses:
*       201:
*         schema:
*           type: object
*           properties:
*             id:
*               type: integer
*             name:
*               type: string
*             description:
*               type: string
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.post('/', journalController.createJournal);

/**
* @swagger
* /journal/{id}:
*   put:
*     summary: Update a journal
*     tags:
*       - journal
*     produces:
*       - application/json
*     parameters:
*       - name: X-Access-Token
*         in: header
*         required: true
*         type: string
*       - name: id
*         in: path
*         required: true
*         type: string
*       - name: name
*         in: formData
*         required: false
*         type: string
*       - name: description
*         in: formData
*         required: false
*         type: string
*     responses:
*       200:
*         schema:
*           type: object
*           properties:
*             id:
*               type: integer
*             name:
*               type: string
*             description:
*               type: string
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.put('/:id', journalController.updateJournal);

/**
* @swagger
* /journal/{id}:
*   delete:
*     summary: Delete the journal
*     tags:
*       - journal
*     produces:
*       - application/json
*     parameters:
*       - name: X-Access-Token
*         in: header
*         required: true
*         type: string
*       - name: id
*         in: path
*         required: true
*         type: string
*     responses:
*       204:
*         description: The resource was deleted successfully.
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.delete('/:id', journalController.deleteJournal);

module.exports = router;
