const router = require('express-promise-router')();

const userController = require('../controllers/user.controller');

/**
* @swagger
* /user:
*   get:
*     summary: Get user profile
*     tags:
*       - user
*     produces:
*       - application/json
*     parameters:
*       - name: X-Access-Token
*         in: header
*         required: true
*         type: string
*     responses:
*       200:
*         schema:
*           type: object
*           properties:
*             id:
*               type: integer
*             email:
*               type: string
*             name:
*               type: string
*             surname:
*               type: string
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.get('/', userController.getUser);

/**
* @swagger
* /user/search:
*   get:
*     summary: Find user
*     tags:
*       - user
*     produces:
*       - application/json
*     parameters:
*       - name: X-Access-Token
*         in: header
*         required: true
*         type: string
*       - name: value
*         in: query
*       - name: journalId
*         in: query
*       - name: size
*         in: query
*     responses:
*       200:
*         schema:
*           type: array
*           items:
*             type: object
*             properties:
*               id:
*                 type: integer
*               email:
*                 type: string
*               name:
*                 type: string
*               surname:
*                 type: string
*               icon:
*                 type: string
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.get('/search', userController.findUser);

/**
* @swagger
* /user/journal:
*   get:
*     summary: Get user journals
*     tags:
*       - user
*     produces:
*       - application/json
*     parameters:
*       - name: X-Access-Token
*         in: header
*         required: true
*         type: string
*     responses:
*       200:
*         schema:
*           type: array
*           items:
*             type: object
*             properties:
*               id:
*                 type: integer
*               name:
*                 type: string
*               description:
*                 type: string
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.get('/journal', userController.getJournals);

/**
* @swagger
* /user/journal/{id}:
*   post:
*     summary: Add member to journal
*     tags:
*       - user
*     produces:
*       - application/json
*     parameters:
*       - name: X-Access-Token
*         in: header
*         required: true
*         type: string
*       - name: userId
*         in: formData
*         required: true
*         type: number
*       - name: id
*         in: path
*         required: true
*         type: number
*     responses:
*       204:
*         description: The user was added to journal successfully.
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.post('/journal/:id', userController.addMemeberToJournal);

/**
* @swagger
* /user/journal/{id}:
*   delete:
*     summary: Remove member from journal
*     tags:
*       - user
*     produces:
*       - application/json
*     parameters:
*       - name: X-Access-Token
*         in: header
*         required: true
*         type: string
*       - name: userId
*         in: query
*         required: true
*         type: number
*       - name: id
*         in: path
*         required: true
*         type: number
*     responses:
*       204:
*         description: The user was added to journal successfully.
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.delete('/journal/:id', userController.removeMemeberFromJournal);

/**
* @swagger
* /user/journal/{id}/members:
*   get:
*     summary: Get members for journal
*     tags:
*       - user
*     produces:
*       - application/json
*     parameters:
*       - name: X-Access-Token
*         in: header
*         required: true
*         type: string
*       - name: id
*         in: path
*         required: true
*         type: number
*     responses:
*       200:
*         schema:
*           type: array
*           items:
*             type: object
*             properties:
*               id:
*                 type: integer
*               email:
*                 type: string
*               name:
*                 type: string
*               surname:
*                 type: string
*               icon:
*                 type: string
*               isOwner:
*                 type: boolean
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.get('/journal/:id/members', userController.getMembers);

/**
* @swagger
* /user/journal/{id}/marks:
*   get:
*     summary: Get marks for journal
*     tags:
*       - user
*     produces:
*       - application/json
*     parameters:
*       - name: X-Access-Token
*         in: header
*         required: true
*         type: string
*       - name: id
*         in: path
*         required: true
*         type: number
*     responses:
*       200:
*         schema:
*           type: array
*           items:
*             type: object
*             properties:
*               id:
*                 type: integer
*               email:
*                 type: string
*               name:
*                 type: integer
*               surname:
*                 type: string
*               icon:
*                 type: string
*               marks:
*                 type: array
*                 items:
*                   type: object
*                   properties:
*                       date:
*                           type: string
*                           format: date
*                       markId:
*                           type: number
*                       value:
*                           type: number
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.get('/journal/:id/marks', userController.getMarks);

/**
* @swagger
* /user/journal/{id}/lessons:
*   get:
*     summary: Get lessons for journal
*     tags:
*       - user
*     produces:
*       - application/json
*     parameters:
*       - name: X-Access-Token
*         in: header
*         required: true
*         type: string
*       - name: id
*         in: path
*         required: true
*         type: number
*     responses:
*       200:
*         schema:
*           type: array
*           items:
*             type: object
*             properties:
*               id:
*                 type: integer
*               date:
*                 type: string
*                 format: date
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.get('/journal/:id/lessons', userController.getLessons);

/**
* @swagger
* /user/email:
*   put:
*     summary: Update user email
*     tags:
*       - user
*     produces:
*       - application/json
*     parameters:
*       - name: X-Access-Token
*         in: header
*         required: true
*         type: string
*       - name: email
*         in: formData
*         required: true
*         type: string
*     responses:
*       200:
*         description: The email was updated successfully.
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.put('/email', userController.changeEmail);

/**
* @swagger
* /user/password:
*   put:
*     summary: Update user password
*     tags:
*       - user
*     produces:
*       - application/json
*     parameters:
*       - name: X-Access-Token
*         in: header
*         required: true
*         type: string
*       - name: password
*         in: formData
*         required: true
*         type: string
*     responses:
*       200:
*         description: The password was updated successfully.
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.put('/password', userController.changePassword);

/**
* @swagger
* /user/icon:
*   put:
*     summary: Update user icon
*     tags:
*       - user
*     produces:
*       - application/json
*     parameters:
*       - name: X-Access-Token
*         in: header
*         required: true
*         type: string
*       - name: icon
*         in: formData
*         required: true
*         type: string
*     responses:
*       200:
*         description: The icon was updated successfully.
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.put('/icon', userController.changeIcon);

/**
* @swagger
* /user:
*   put:
*     summary: Update user data
*     tags:
*       - user
*     produces:
*       - application/json
*     parameters:
*       - name: X-Access-Token
*         in: header
*         required: true
*         type: string
*       - name: icon
*         in: formData
*         required: true
*         type: string
*       - name: name
*         in: formData
*         required: true
*         type: string
*       - name: surname
*         in: formData
*         required: true
*         type: string
*       - name: email
*         in: formData
*         required: true
*         type: string
*     responses:
*       200:
*         description: The user was updated successfully.
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.put('/', userController.changeUser);

module.exports = router;
