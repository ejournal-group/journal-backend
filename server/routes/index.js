const router = require('express-promise-router')();

// routes
const authRoutes = require('./auth.routes');
const journalRoutes = require('./journal.routes');
const userRoutes = require('./user.routes');
const markRoutes = require('./mark.routes');

// middlewares
const isLoginMiddleware = require('../middlewares/is-login.middleware');
const serializeMiddleware = require('../middlewares/serialize.middleware');

/**
 * @swagger
 * tags:
 *   - name: auth
 *     description: Authentication controller
 */
router.use('/auth', authRoutes);

/**
 * @swagger
 * tags:
 *   - name: journal
 *     description: Journal controller
 */
router.use('/journal', serializeMiddleware, isLoginMiddleware, journalRoutes);

/**
 * @swagger
 * tags:
 *   - name: user
 *     description: User controller
 */
router.use('/user', serializeMiddleware, isLoginMiddleware, userRoutes);

/**
 * @swagger
 * tags:
 *   - name: mark
 *     description: Mark controller
 */
router.use('/mark', serializeMiddleware, isLoginMiddleware, markRoutes);

module.exports = router;
