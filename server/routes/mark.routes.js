const router = require('express-promise-router')();

const markController = require('../controllers/mark.controller');

/**
* @swagger
* /mark:
*   post:
*     summary: Create a journal column
*     tags:
*       - mark
*     produces:
*       - application/json
*     parameters:
*       - name: X-Access-Token
*         in: header
*         required: true
*         type: string
*       - name: journalId
*         in: formData
*         required: true
*         type: number
*       - name: date
*         in: formData
*         required: true
*         type: string
*         format: date
*     responses:
*       201:
*         schema:
*           type: object
*           properties:
*             id:
*               type: integer
*             date:
*               type: string
*               format: date
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.post('/', markController.createLesson);

/**
* @swagger
* /mark/{journalId}:
*   post:
*     summary: Add a journal mark
*     tags:
*       - mark
*     produces:
*       - application/json
*     parameters:
*       - name: X-Access-Token
*         in: header
*         required: true
*         type: string
*       - name: lessonId
*         in: formData
*         required: true
*         type: number
*       - name: userId
*         in: formData
*         required: true
*         type: number
*       - name: value
*         in: formData
*         required: true
*         type: number
*       - name: journalId
*         in: path
*         required: true
*         type: string
*     responses:
*       201:
*         schema:
*           type: object
*           properties:
*             id:
*               type: integer
*             value:
*               type: string
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.post('/:journalId', markController.addMark);

/**
* @swagger
* /mark/{journalId}:
*   put:
*     summary: Update a journal mark
*     tags:
*       - mark
*     produces:
*       - application/json
*     parameters:
*       - name: X-Access-Token
*         in: header
*         required: true
*         type: string
*       - name: markId
*         in: formData
*         required: true
*         type: number
*       - name: value
*         in: formData
*         required: true
*         type: number
*       - name: journalId
*         in: path
*         required: true
*         type: string
*     responses:
*       200:
*         schema:
*           type: object
*           properties:
*             id:
*               type: integer
*             value:
*               type: string
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.put('/:journalId', markController.updateMark);

/**
* @swagger
* /mark/{journalId}:
*   delete:
*     summary: Remove a journal mark
*     tags:
*       - mark
*     produces:
*       - application/json
*     parameters:
*       - name: X-Access-Token
*         in: header
*         required: true
*         type: string
*       - name: markId
*         in: formData
*         required: true
*         type: number
*       - name: journalId
*         in: path
*         required: true
*         type: string
*     responses:
*       204:
*         description: The resource was deleted successfully.
*       400:
*         schema:
*           type: object
*           properties:
*             error:
*               type: string
*               description: Error message
*/
router.delete('/:journalId', markController.deleteMark);

module.exports = router;
