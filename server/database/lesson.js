module.exports = (sequelize, DataTypes) => {
    const Lesson = sequelize.define('Lesson', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        },
        date: {
            type: DataTypes.DATEONLY,
            allowNull: false,
            validate: {
                notNull: {
                    msg: 'Date is required',
                },
            },
        },
    });

    Lesson.associate = (models) => {
        Lesson.hasMany(models.Mark, {
            as: 'marks',
            foreignKey: 'lessonId',
        });
    };

    return Lesson;
};
