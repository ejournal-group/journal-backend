module.exports = (sequelize, DataTypes) => {
    const Journal = sequelize.define('Journal', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        },
        name: {
            type: DataTypes.STRING(100),
            allowNull: false,
            validate: {
                notNull: {
                    msg: 'Journal name is required',
                },
            },
        },
        description: {
            type: DataTypes.STRING(1000),
            allowNull: false,
            validate: {
                notNull: {
                    msg: 'Journal description is required',
                },
            },
        },
    });

    Journal.associate = (models) => {
        Journal.belongsToMany(models.User, {
            through: models.UserJournal,
            as: 'users',
            foreignKey: 'journalId',
        });
        Journal.hasMany(models.Lesson, {
            foreignKey: 'journalId',
        });
    };

    return Journal;
};
