module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        },
        email: {
            type: DataTypes.STRING(100),
            allowNull: false,
            unique: {
                msg: 'Email is already in use.',
                fields: ['email'],
            },
            validate: {
                notNull: {
                    msg: 'Email is required',
                },
            },
        },
        password: {
            type: DataTypes.STRING(80),
            allowNull: false,
            validate: {
                notNull: {
                    msg: 'Password is required',
                },
            },
        },
        name: {
            type: DataTypes.STRING(50),
            allowNull: false,
            validate: {
                notNull: {
                    msg: 'User name is required',
                },
            },
        },
        surname: {
            type: DataTypes.STRING(50),
            allowNull: false,
            validate: {
                notNull: {
                    msg: 'User surname is required',
                },
            },
        },
        icon: {
            type: DataTypes.STRING(2000),
            allowNull: true,
        },
    });

    User.associate = (models) => {
        User.belongsToMany(models.Journal, {
            through: models.UserJournal,
            as: 'journals',
            foreignKey: 'userId',
        });
    };

    return User;
};
