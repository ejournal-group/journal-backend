module.exports = (sequelize, DataTypes) => {
    const UserJournal = sequelize.define('UserJournal', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        },
        isOwner: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            validate: {
                notNull: {
                    msg: 'IsOwner is required',
                },
            },
        },
    });

    UserJournal.associate = (models) => {
        UserJournal.hasMany(models.Mark, {
            foreignKey: 'userJournalId',
        });
    };

    return UserJournal;
};
