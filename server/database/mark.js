module.exports = (sequelize, DataTypes) => {
    const Mark = sequelize.define('Mark', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        },
        value: {
            type: DataTypes.STRING(10),
        },
        userJournalId: {
            type: DataTypes.BIGINT,
            allowNull: false,
        },
    });

    Mark.associate = () => {
        // Mark.hasOne(models.UserJournal, {
        //     sourceKey: 'userJournalId',
        //     foreignKey: 'id',
        // });
    };

    return Mark;
};
