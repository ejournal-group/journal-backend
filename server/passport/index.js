
// hashing password
const bcrypt = require('bcryptjs');

// passport
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const db = require('../database');

passport.use('login',
    new LocalStrategy({ passReqToCallback: true, usernameField: 'email' }, (
        async (req, username, password, done) => {
            try {
                const userPassword = password.trim();

                const user = await db.User.findOne({
                    where: { email: username },
                });

                if (!user) {
                    throw new Error('You entered an invalid email.');
                }
                const { id, password: passwordFromDb } = user;

                if (!bcrypt.compareSync(userPassword, passwordFromDb)) {
                    throw new Error('You entered an invalid password.');
                }

                return done(null, id);
            } catch (error) {
                return done(error, null);
            }
        }
    )));

passport.use('signup',
    new LocalStrategy({ passReqToCallback: true, usernameField: 'email' }, (
        async (req, username, password, done) => {
            try {
                const { name, surname } = req.body;

                const passwordHash = bcrypt.hashSync(password.trim(), 10);

                const newUser = {
                    email: username,
                    name,
                    surname,
                    password: passwordHash,
                };

                const data = await db.User.create(newUser);

                return done(null, data.id);
            } catch (error) {
                return done(error, null);
            }
        }
    )));

module.exports = passport;
