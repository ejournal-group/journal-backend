
const { verify } = require('../utils/jwt');

const userService = require('../services/user.service');

const serializeUser = async (req, res, next) => {
    const token = req.get('X-Access-Token');

    if (token) {
        try {
            const decoded = await verify(token);
            const userData = await userService.getById(decoded.id);
            req.user = userData;
            return next();
        } catch (error) {
            return next();
        }
    } else {
        return next();
    }
};

module.exports = serializeUser;
