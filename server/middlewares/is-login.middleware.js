const HttpError = require('../utils/error');

const isLogin = async (req, res, next) => {
    if (req.isAuthenticated()) {
        return next();
    }
    return next(new HttpError(401, 'Unauthorized'));
};

module.exports = isLogin;
