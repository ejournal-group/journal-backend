const HttpError = require('../utils/error');

// next is required here else it isn't error handler
module.exports = (err, req, res, next) => {
    if (err instanceof HttpError) {
        res.status(err.status);
    } else {
        res.status(500);
    }

    res.send({ error: err.message });
};
