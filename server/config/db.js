module.exports = {
    development: {
        dialect: 'postgres',
        url: 'localhost:5432',
        database: 'ejournal',
        username: 'postgres',
        password: 'admin',
    },
    test: {
    },
    production: {
        dialect: 'postgres',
        use_env_variable: 'DATABASE_URL',
    },
};
