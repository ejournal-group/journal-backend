const HttpError = require('../utils/error');

const markService = require('../services/mark.service');

async function createLesson(req, res, next) {
    try {
        const { id: userId } = req.user;
        const { journalId, date } = req.body;
        const { id: lessonId, date: lessonDate } = await markService.createLesson(journalId, date, userId);
        return res.status(201).json({ id: lessonId, date: lessonDate });
    } catch (error) {
        return next(new HttpError(400, error.message));
    }
}

async function addMark(req, res, next) {
    try {
        const { id: ownerId } = req.user;
        const { journalId } = req.params;
        const { lessonId, userId, value } = req.body;
        const data = await markService.addMark(journalId, userId, lessonId, value, ownerId);
        return res.status(201).json(data);
    } catch (error) {
        return next(new HttpError(400, error.message));
    }
}

async function updateMark(req, res, next) {
    try {
        const { id: ownerId } = req.user;
        const { journalId } = req.params;
        const { markId, value } = req.body;
        const data = await markService.updateMark(journalId, markId, value, ownerId);
        return res.status(200).json(data);
    } catch (error) {
        return next(new HttpError(400, error.message));
    }
}

async function deleteMark(req, res, next) {
    try {
        const { id: ownerId } = req.user;
        const { journalId } = req.params;
        const { markId } = req.body;
        await markService.removeMark(journalId, markId, ownerId);
        return res.status(204).send();
    } catch (error) {
        return next(new HttpError(400, error.message));
    }
}

module.exports = {
    createLesson,
    addMark,
    updateMark,
    deleteMark,
};
