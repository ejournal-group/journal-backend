const HttpError = require('../utils/error');

const journalService = require('../services/journal.service');

async function getJournal(req, res, next) {
    try {
        const { id: userId } = req.user;
        const journalId = req.params.id;
        const data = await journalService.getOne(journalId, userId);
        if (!data) {
            return next(new HttpError(404, 'Wrong journal id'));
        }
        return res.status(200).json(data);
    } catch (error) {
        return next(new HttpError(400, error.message));
    }
}

async function createJournal(req, res, next) {
    try {
        const { id } = req.user;
        const { name, description } = req.body;

        if (!name) {
            return next(new HttpError(400, 'Name is required'));
        }

        if (!description) {
            return next(new HttpError(404, 'Description is required'));
        }

        const data = await journalService.create(id, name, description);
        return res.status(201).json(data);
    } catch (error) {
        return next(new HttpError(400, error.message));
    }
}

async function updateJournal(req, res, next) {
    try {
        const { id: userId } = req.user;
        const { id: journalId } = req.params;
        const { id, ...dataToUpdate } = req.body;
        const data = await journalService.update(journalId, dataToUpdate, userId);
        if (!data) {
            return next(new HttpError(400, 'Wrong data'));
        }
        return res.status(200).json(data);
    } catch (error) {
        return next(new HttpError(400, error.message));
    }
}

async function deleteJournal(req, res, next) {
    try {
        const journalId = req.params.id;
        const data = await journalService.remove(journalId);
        if (!data) {
            return next(new HttpError(404, 'Wrong journal id'));
        }
        return res.status(204).json();
    } catch (error) {
        return next(new HttpError(400, error.message));
    }
}

module.exports = {
    getJournal,
    createJournal,
    updateJournal,
    deleteJournal,
};
