const HttpError = require('../utils/error');

const userService = require('../services/user.service');
const journalService = require('../services/journal.service');

async function getUser(req, res, next) {
    try {
        const { id: userId } = req.user;
        const data = await userService.getById(userId);
        if (!data) {
            return next(new HttpError(400, 'Wrong data'));
        }
        return res.status(200).json(data);
    } catch (error) {
        return next(new HttpError(400, error.message));
    }
}

async function findUser(req, res, next) {
    try {
        const { value, journalId, size } = req.query;
        const data = await userService.findUser(value, journalId, size);
        if (!data) {
            return next(new HttpError(400, 'Wrong data'));
        }
        return res.status(200).json(data);
    } catch (error) {
        return next(new HttpError(400, error.message));
    }
}

async function getJournals(req, res, next) {
    try {
        const { id: userId } = req.user;
        const data = await journalService.getForUser(userId);
        if (!data) {
            return next(new HttpError(400, 'Wrong data'));
        }
        return res.status(200).json(data);
    } catch (error) {
        return next(new HttpError(400, error.message));
    }
}

async function addMemberToJournal(req, res, next) {
    try {
        const { id: ownerId } = req.user;
        const { id: projectId } = req.params;
        const { userId } = req.body;
        const data = await journalService.getForUser(ownerId);
        if (!data || !data.length) {
            return next(new HttpError(400, 'Wrong data'));
        }
        const project = data.find((journal) => journal.id === projectId);
        if (!project || !project.isOwner) {
            return next(new HttpError(400, 'No access'));
        }
        await journalService.addMember(projectId, userId);
        return res.status(200).send();
    } catch (error) {
        return next(new HttpError(400, error.message));
    }
}

async function getMembers(req, res, next) {
    try {
        const { id: userId } = req.user;
        const { id: journalId } = req.params;
        const data = await journalService.getMembers(journalId, userId);
        if (!data) {
            return next(new HttpError(400, 'Wrong data'));
        }
        return res.status(200).json(data);
    } catch (error) {
        return next(new HttpError(400, error.message));
    }
}

async function getMarks(req, res, next) {
    try {
        const { id: userId } = req.user;
        const { id: journalId } = req.params;
        const data = await journalService.getMarks(journalId, userId);
        if (!data) {
            return next(new HttpError(400, 'Wrong data'));
        }
        return res.status(200).json(data);
    } catch (error) {
        return next(new HttpError(400, error.message));
    }
}

async function getLessons(req, res, next) {
    try {
        const { id: userId } = req.user;
        const { id: journalId } = req.params;
        const data = await journalService.getLessons(journalId, userId);
        if (!data) {
            return next(new HttpError(400, 'Wrong data'));
        }
        return res.status(200).json(data);
    } catch (error) {
        return next(new HttpError(400, error.message));
    }
}


async function removeMemberFromJournal(req, res, next) {
    try {
        const { id: ownerId } = req.user;
        const { id: projectId } = req.params;
        const { userId } = req.query;
        const data = await journalService.getForUser(ownerId);
        if (!data || !data.length) {
            return next(new HttpError(400, 'Wrong data'));
        }
        const project = data.find((journal) => journal.id === projectId);
        if (!project || !project.isOwner) {
            return next(new HttpError(400, 'No access'));
        }
        await journalService.removeMember(projectId, userId);
        return res.status(204).send();
    } catch (error) {
        return next(new HttpError(400, error.message));
    }
}

async function changeEmail(req, res, next) {
    try {
        const { id: userId } = req.user;
        const { email } = req.body;
        const data = await userService.changeEmail(userId, email);
        if (!data) {
            return next(new HttpError(400, 'Wrong data'));
        }
        return res.status(200).json(data);
    } catch (error) {
        return next(new HttpError(400, error.message));
    }
}

async function changePassword(req, res, next) {
    try {
        const { id: userId } = req.user;
        const { password } = req.body;
        const data = await userService.changePassword(userId, password);
        if (!data) {
            return next(new HttpError(400, 'Wrong data'));
        }
        return res.status(200).json(data);
    } catch (error) {
        return next(new HttpError(400, error.message));
    }
}

async function changeIcon(req, res, next) {
    try {
        const { id: userId } = req.user;
        const { icon } = req.body;
        const data = await userService.changeIcon(userId, icon);
        if (!data) {
            return next(new HttpError(400, 'Wrong data'));
        }
        return res.status(200).json(data);
    } catch (error) {
        return next(new HttpError(400, error.message));
    }
}

async function changeUser(req, res, next) {
    try {
        const { id: userId } = req.user;
        const {
            icon, name, surname, email,
        } = req.body;
        const data = await userService.changeUser(userId, icon, name, surname, email);
        if (!data) {
            return next(new HttpError(400, 'Wrong data'));
        }
        return res.status(200).json(data);
    } catch (error) {
        return next(new HttpError(400, error.message));
    }
}

module.exports = {
    getUser,
    findUser,
    getJournals,
    addMemeberToJournal: addMemberToJournal,
    removeMemeberFromJournal: removeMemberFromJournal,
    getMembers,
    getMarks,
    getLessons,
    changeEmail,
    changePassword,
    changeIcon,
    changeUser,
};
