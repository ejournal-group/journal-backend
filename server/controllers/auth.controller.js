const passport = require('passport');
const HttpError = require('../utils/error');

const authService = require('../services/auth.service');

async function register(req, res, next) {
    const { email, password } = req.body;

    if (!email) {
        return next(new HttpError(400, 'Email is required.'));
    }

    if (!password) {
        return next(new HttpError(400, 'Password is required.'));
    }

    return passport.authenticate('signup', (err, userId) => {
        if (err) {
            return next(new HttpError(400, err.message));
        }
        req.user = { id: userId };
        return next();
    })(req, res, next);
}

async function login(req, res, next) {
    const { email, password } = req.body;

    if (!email) {
        return next(new HttpError(400, 'Email is required.'));
    }

    if (!password) {
        return next(new HttpError(400, 'Password is required.'));
    }

    return passport.authenticate('login', (err, userId) => {
        if (err) {
            return next(new HttpError(400, err.message));
        }
        req.user = { id: userId };
        return next();
    })(req, res, next);
}

async function generateTokens(req, res, next) {
    const { user } = req;
    const { id } = user;
    const tokens = await authService.generateTokens(id);
    res.json(tokens);
}

async function refreshToken(req, res, next) {
    const { refreshToken: token } = req.body;

    if (!token) {
        return next(new HttpError(400, 'Token is required'));
    }

    const tokens = await authService.refreshAccessToken(token);
    res.json(tokens);
}

module.exports = {
    login,
    register,
    generateTokens,
    refreshToken,
};
